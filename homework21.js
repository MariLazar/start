"use strict"

//упражнение 1
/**
 * проверяет наличие данных в объекте
 * @param {object} user -объект с данными
 * @return {boolean} 
 */

let user = {};

function isEmpty(obj) {

    for (let key in obj) {
        return false;
    }

    return true;
};

console.log(isEmpty(user));


//упражнение 3

/**
 * считает увеличение зарплаты в %
 * @param {object} salaries -начальная зарплата
 * @param {object} newSalary -увеличенная зарплата
 * @param {number} percent - процент увеличения
 * @return {object} newSalary- возвращает увеличенную зарплату
 */

let salaries = {
    John: 100000,
    Ann: 160000,
    Pete: 130000,
};

let newSalary = {};
function raiseSalary(percent) {

    for (let key in salaries) {
        newSalary[key] = Math.floor(salaries[key] + (salaries[key] / 100 * percent));

    };
    return newSalary;

};

console.log(newSalary);

raiseSalary(80);


/**
 * выводит общую сумму зарплаты
 * @param {number} sum -общая сумма
 * @param {object} newSalary -увеличенная зарплата
 * @return {number} sum- возвращает общую сумму*/

let sum = 0;
function getAllSalaries(salaries) {
    for (let key in newSalary) {

        sum += newSalary[key];

    }
    return sum
}

console.log(getAllSalaries(newSalary))