"use strict"

//упражнение 1
/** вычисляет сумму чисел в массиве
 * @param (array) arr - массив значений
 * @return (number) sum - сумма числовых значений в массиве
 */

function getSumm(arr) {
  let sum = 0;
  for (let n = 0; n < arr.length; n++) {

    if (typeof arr[n] !== "number") continue;

    sum += arr[n];
  }
  return sum;
}
let arr1 = [3, {}, 3, -2, 28, "a"];
let arr2 = ["b", 9, 3, -2, 12, "a"];//добавлена для проверки правильности работы функции
let result = getSumm(arr1)

getSumm(arr1)

console.log(result)

//упражнение 2


let cart = [];


/** добавляет в массив уникальные элементы
 * @param (index) id - элемент в массиве
 * @return (array) cart - возвращает массив в корзину
 */

function addCart(id) {
  if (!cart.includes(id)){
  cart.push (id)
}
return cart;
}

addCart (8456)
addCart (8456)
addCart (2217)
addCart (5473)
addCart (9632)

console.log (cart)

/** удаляет из массива элемент
 * @param (index) id - элемент в массиве
 * @return (array) cart - возвращает массив в корзину
 */
function removeFromCart (id) {
let index =cart.indexOf(id);
if (index>=0) {
  cart.splice (index,1);
}
return cart;
}
removeFromCart (2217)
console.log (cart)